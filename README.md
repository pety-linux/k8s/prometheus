# prometheus



## Getting started:
```
www.fosslinux.com/10398/how-to-install-and-configure-prometheus-on-centos-7.htm
```

### Docker daemon.json
```
{
  "metrics-addr" : "127.0.0.1:9323",
  "experimental" : true
}
```

### cadvisor
``` 
version: '3.4'
services:
  cadvisor:
    image: gcr.io/cadvisor/cadvisor
    container_name: cadvisor
    privileged: true
    devices:
      - "/dev/kmsg:/dev/kmsg"
    volumes:
      - /:/rootfs:ro
      - /var/run:/var/run:ro
      - /sys:/sys:ro
      - /var/lib/docker/:/var/lib/docker:ro
      - /dev/disk/:/dev/disk:ro
    ports:
      - 8070:8080
```

#### prometheus.yml
```
  - job_name: "nodes"
    static_configs:
      - targets: ["node01:9100", "node02:9100"]
  - job_name: "docker"
    static_configs:
            - targets: ["localhost:9323"]
```
