#!/bin/bash

# Variables
version=1.9.0
user=ipmi_exporter
# Add user
useradd --no-create-home --shell /bin/false ${user}
# Download binary
wget https://github.com/prometheus-community/ipmi_exporter/releases/download/v${version}/ipmi_exporter-${version}.linux-amd64.tar.gz
# Extract tar file
tar -xvzf ipmi_exporter-*.tar.gz
# Copy to binaries
cp ipmi_exporter-${version}.linux-amd64/ipmi_exporter /usr/local/bin/ && chown ${user}:${user} /usr/local/bin/ipmi_exporter
# Create systemd service
cat << EOF > /etc/systemd/system/ipmi_exporter.service
[Unit]
Description=Node Exporter
Wants=network-online.target
After=network-online.target

[Service]
User=${user}
Group=${user}
Type=simple
#ExecStart=/usr/local/bin/ipmi_exporter --config.file=/opt/prometheus/ipmi_exporter/config/ipmi_exporter_local.yml
ExecStart=/usr/local/bin/ipmi_exporter

[Install]
WantedBy=multi-user.target
EOF
# Restart service
systemctl daemon-reload
systemctl enable ipmi_exporter --now
# Cleanup
rm -rf ipmi_exporter*