#!/bin/bash

# Variables
version=1.8.2
user=node_exporter
# Add user
useradd --no-create-home --shell /bin/false ${user}
# Download binary
wget https://github.com/prometheus/node_exporter/releases/download/v${version}/node_exporter-${version}.linux-amd64.tar.gz
# Extract tar file
tar -xvzf node_exporter-*.tar.gz
# Copy to binaries
cp node_exporter-${version}.linux-amd64/node_exporter /usr/local/bin/ && chown ${user}:${user} /usr/local/bin/node_exporter
# Create systemd service
cat << EOF > /etc/systemd/system/node_exporter.service
[Unit]
Description=Node Exporter
Wants=network-online.target
After=network-online.target

[Service]
User=${user}
Group=${user}
Type=simple
ExecStart=/usr/local/bin/node_exporter

[Install]
WantedBy=multi-user.target
EOF
# Restart service
systemctl daemon-reload
systemctl enable node_exporter --now
# Cleanup
rm -rf node_exporter*
